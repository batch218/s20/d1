// [SECTION] While Loop

/*
	- A while loop takes in an expression/condition	
	- Expression are any unit of code that can evaluated to a value
	- If the condition evaluates to true, the statements inside the block will be executed.

*/

/*let count = 5;

while (count!== 0) {
  console.log("Count:" + count);
  count-- ; // decrements count value // count=count -1;
  
}
console.log("=======================")*/
// [SECTION] Do While

							//parseInt 
// let number = Number(prompt("Give me a number: "))
// The "Number" function works similar to the "parseInt" functionn
// excecute the program first before the condition while
// After excecuting once, the while statement will evaluate whether to run the next iteraton of the loop based on iven expression/condition is not true(false), the loop will stop

/*do{
	console.log("Number: " + number);
	number++ ; //increments value into 1
}while(number<10)//number must be less than 10*/

console.log("=======================");
/*let  n = 15;

while(number<10){
	console.log("number " + number);
	number++;
}*/

// [SECTION] For loop
		// Initiallization //condition //change of value
for (let count = 0; count<=5; count++ ){
	console.log(count)
};
console.log("=======================");
let myString = "Jhun";
/*
j index = 0
H index = 1
U index = 2
N index = 3
*/

console.log("myString length: " + myString.length);
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);
console.log("=======================");
															//change of value
for(i=0; i < myString.length; i++){
	console.log(myString[i]);
}

let myName = "AlEx";

for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a"||
		myName[i].toLowerCase() == "e"||
		myName[i].toLowerCase() == "i"||
		myName[i].toLowerCase() == "o"||
		myName[i].toLowerCase() == "u"
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}
